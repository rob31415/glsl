#version 330

in vec4 color;
out vec4 outColor;
in float time;

void main() {
   outColor = vec4(.5, 0.2, 0.1+sin(time), 1.0);  // /abs(sin(time/0.5));
}