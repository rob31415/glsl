#version 330

uniform mat4 matVP;
uniform mat4 matGeo;

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;

out vec4 color;
in float time;

void main() {
	vec3 n2 = vec3(normal.x, normal.y, normal.z);
	//vec3 n2 = vec3(sin(time), cos(time), sin(time));
	color = vec4(n2, 1.0);
	gl_Position = matVP * matGeo * vec4(pos, 1);
}
